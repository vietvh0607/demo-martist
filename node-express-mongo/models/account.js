var mongoose = require('mongoose');
var Ob
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

const AccountSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    }
}, {
    collection: 'Account'
});


AccountSchema.plugin(passportLocalMongoose);

AccountSchema.methods.register = function (account, password, cb) {
    account.password = password;
    AccountSchema.collection.insert(account, cb);
}

//AccountSchema.methods.authenticate = function (username, password, done) {
//    AccountSchema.find({
//        'username': username
//    }, function (user) {
//        bcrypt.compare(password, user.password, function (err, result) {
//            if (err) {
//                return done(err);
//            }
//            if (!result) {
//                return done(null, false, {
//                    message: 'Incorrect username and password'
//                });
//            }
//            return done(null, user);
//        })
//    }).catch(function (err) {
//        return done(err);
//    })
//}
//
//AccountSchema.methods.serializeUser = function (account, done) {
//    done(null, user.id);
//}
//
//AccountSchema.methods.deserializeUser = function (id, done) {
//    db.user.findById(id).then(function (user) {
//        done(null, user);
//    }).catch(function (err) {
//        console.log(err);
//    })
//}




module.exports = mongoose.model('Account', AccountSchema);
