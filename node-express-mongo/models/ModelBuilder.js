var Content = require('../models/content');


var ModelBuilder = {
    createContent: function (_id,title, description, author) {
        let content = new Content({
            id: _id,
            title: title,
            description: description,
            author: author
        })
        return content;
    }

}

module.exports = ModelBuilder;
