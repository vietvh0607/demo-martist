var mongoose = require('mongoose');

var Schema = mongoose.Schema;
const ContentSchema = new Schema({
    id: {
        type: String,
        required: false
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    }
}, {
    collection: 'Content'
});

ContentSchema.methods.createContent = function (content, cb) {
    ContentSchema.collection.insert(content, cb);
}

module.exports = mongoose.model('Content', ContentSchema);
