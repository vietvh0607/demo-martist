var contentSchema = require('../models/content');
var ModelBuilder = require('../models/ModelBuilder');

var ContentCollection = {
    deleteContent: function (req, res) {
        //        console.log(req.body._id);
        contentSchema.remove({
            _id: req.body._id
        }, function (err) {
            if (err) {
                res.send(err);
            }
            contentSchema.find(function (err, contents) {
                let result = [];
                for (let i = 0; i < contents.length; i++) {
                    result.push(new ModelBuilder.createContent(
                        contents[i]._id,
                        contents[i].title,
                        contents[i].description,
                        contents[i].author
                    ));
                }
                res.send(result);
            })
        });
    }
}

module.exports = ContentCollection;
