$('.btnRegister').on('click', function (e) {
    e.preventDefault();
    let username = $('.register-username').val().trim();
    let flag = true;
    if (checkUsername(username)) {
        $('.register-err-username').css({
            'display': 'none'
        })
    } else {
        err = false;
        $('.register-err-username').css({
            'display': 'block'
        })
    }
    let fullname = $('.register-fullname').val().trim();
    if (checkFullname(fullname)) {
        $('.register-err-fullname').css({
            'display': 'none'
        })
    } else {
        err = false;
        $('.register-err-fullname').css({
            'display': 'block'
        })
    }
    let address = $('.register-address').val().trim();
    if (checkAddress(address)) {
        $('.register-err-address').css({
            'display': 'none'
        })
    } else {
        err = false;
        $('.register-err-address').css({
            'display': 'block'
        })
    }
    let phone = $('.register-phone').val().trim();
    if (checkPhone(phone)) {
        $('.register-err-phone').css({
            'display': 'none'
        })
    } else {
        err = false;
        $('.register-err-phone').css({
            'display': 'block'
        })
    }
    let password = $('.register-password').val().trim();
    if (checkPassword(password)) {
        $('.register-err-password').css({
            'display': 'none'
        })
    } else {
        err = false;
        $('.register-err-password').css({
            'display': 'block'
        })
    }
    if (flag) {
        $.ajax({
            url: '/register',
            method: 'POST',
            data: {
                'username': username,
                'password': password
            },
            error: function (err) {
                console.log(err);
            },
            success: function (data) {
                console.log("data: " + data);
                window.location = '/login';
            }
        })
    }
})


var checkUsername = function (username) {
    return /^[\w.]{6,30}$/g.test(username);
}

var checkFullname = function (fullname) {
    return /^[a-zA-Z ]{3,}$/g.test(fullname);
}
var checkAddress = function (address) {
    return /^[\w\d, /]{3,}$/g.test(address);
}
var checkPhone = function (phone) {
    return /^[\d]{10,11}$/g.test(phone);
}
var checkPassword = function (password) {
    return /(?=^[0-9a-zA-Z!@#$%^&*]{8,32}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g.test(password);
}

$('.update-content').on('click', function (e) {

})

$('.delete-content').on('click', function (e) {
    e.preventDefault();
    let _id = this.value;
    $.ajax({
        url: '/removeContent',
        method: 'POST',
        data: {
            '_id': _id
        },
        error: function (err) {
            console.log(err);
        },
        success: function (data) {
            reRender(data);
        }
    })
})

//<div class="caption">
//        <h3>{{ this.title }}</h3>
//        <br>
//        <small> {{ this.author}} </small>
//        <br>
//        <form class="form-inline">
//            <div class="form-group mx-sm-3">
//                <input type="text" class="form-control" value="{{ this.description }}" id="content-des">
//            </div>
//            <button type="button" class="btn btn-success update-content">Update</button>
//            <button type="button" class="btn btn-danger delete-content" value="{{ this.id }}">Delete</button>
//        </form>
//    </div>

var reRender = function (list) {
    $('.thumbnail').empty();
    list.forEach(function (el) {
        let caption = $('<div class="caption"></div>');
        let title = $('<h3>' + el.title + '</h3><br><small>' + el.author + '</small><br>');
        caption.append(title);
        let form = $('<form class="form-inline">' +
            '<div class="form-group mx-sm-3">' +
            '<input type="text" class="form-control" value="' + el.description + '" id="content-des">' +
            '</div>' +
            '<button type="button" class="btn btn-success update-content" value="' + el.id + '">Update</button>' +
            '<button type="button" class="btn btn-danger delete-content" value="' + el.id + '">Delete</button>' +
            '</form>');
        caption.append(form);
        $('.thumbnail').append(caption);
    })
}
