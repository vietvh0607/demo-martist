var express = require('express');
var router = express.Router();
var passport = require('passport');
var $ = require('jquery');

var Content = require('../models/content');
var Account = require('../models/account');
var ModelBuilder = require('../models/ModelBuilder');

var jwt = require('jsonwebtoken');
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var ContentCollection = require('../controller/ContentController');

var jwtOptions = {}
jwtOptions.secretOrKey = 'tasmanianDevil';



/* GET home page. */
router.get('/', function (req, res, next) {
    if (req.isAuthenticated()) {
        Content.find(function (err, contents) {
            let result = [];
            for (let i = 0; i < contents.length; i++) {
                result.push(new ModelBuilder.createContent(
                    contents[i]._id,
                    contents[i].title,
                    contents[i].description,
                    contents[i].author
                ));
            }
            res.render('index', {
                title: 'Express',
                contents: result
            });
        })
    } else {
        res.redirect('/login');
    }

});

router.route('/removeContent')
    .get()
    .post(ContentCollection.deleteContent)
    .put()
    .delete()

router.get('/register', function (req, res) {
    res.render('register', {});
});

router.post('/register', function (req, res) {
    Account.register(new Account({
        username: req.body.username
    }), req.body.password, function (err, account) {
        if (err) {
            return res.render('register', {
                account: account
            });
        }
        return res.render('login');

        //        passport.authenticate('local')(req, res, function () {
        //            req.session.save(function (err) {
        //                if (err) {
        //                    return next(err);
        //                }
        //                res.redirect('/');
        //            });
        //        });
    });
});

router.get('/login', function (req, res) {
    res.render('login', {
        user: req.user
    });
});

router.post('/login', passport.authenticate('local'), function (req, res) {
    req.session.save(function (err) {
        if (err) {
            res.redirect('/login', err);
        }
        res.redirect('/');
    });
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/ping', function (req, res) {
    res.status(200).send("pong!");
});

module.exports = router;
